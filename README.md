# Steam Game Finder
Flask Api hosted at http://jewellajacob.pythonanywhere.com/

This API calls the Steam API to return a single, random game that is shared between user id's that are passed in the url.
## Method Handlers
* get_shared_game (get'/users')

  * this accepts steam user ids and returns a random shared game
  * Process:
    * Calls steam API on each id passed to get call to pull user's games -> adds game to shared games list when it occurs in each user -> picks random game to return once shared games list is complete
  * To:Do
    * Handle filters
      * allow user to select time frame, genre 
    * Add better error handling
    * Learn how to seperate file for Flask into a controller - service model
