from GameFinderService import GameFinderService

from flask import Flask
import random
import requests
import ast
import json

app = Flask(__name__)

@app.route('/<users>')
def get_shared_game(users):
    if users == None or users == '':
        return {'game': 'Enter IDs'}
    response = get_random_shared_game(users.split(','))
    return response

def get_owned_games(steam_id):
    steam_url = 'http://api.steampowered.com/' + 'IPlayerService/GetOwnedGames/v1/?key=' + '7F33D6F3F81FB32374405D2B60AAE784' + '&steamid=' + steam_id + '&include_appinfo=true&format=json'
    response = requests.get(steam_url).content
    dict_str = response.decode("UTF-8")

    return json.loads(dict_str)

def get_user_games(user_id):
    game_dict = {}
    response = get_owned_games(user_id)

    for game in response['response']['games']:
        game_dict[game['appid']] = game['name']

    return game_dict

def get_games_overlap(user_ids):
    game_options = {}
    num_users = len(user_ids)
    possible_game_titles = []

    for id in user_ids:
        user_games = get_user_games(id)
        for app_id, name in user_games.items():
            if app_id in game_options:
                game_options[app_id]['occurences'] += 1
            else:
                game_options[app_id] = {'name': name, 'occurences': 1}

    for app_id in game_options:
        if game_options[app_id]['occurences'] == num_users:
            possible_game_titles.append(game_options[app_id]['name'])
    return possible_game_titles

def get_random_shared_game(user_ids):
    if user_ids == None or len(user_ids) < 2:
        return {'game': 'Not enough users entered'}
    else:
        shared_games = get_games_overlap(user_ids)
        print(shared_games)
        if len(shared_games) == 0:
            return 'no shared games'
        elif len(shared_games) > 0:
            random_index = random.randint(0, len(shared_games))
            return {'game': shared_games[random_index]}
        else:
            return {'game': 'unknown error, try again later'}
